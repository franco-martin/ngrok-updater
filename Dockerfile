FROM python
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt && rm /app/requirements.txt
COPY ./main.py /app/main.py
CMD ["python3", "main.py"]
