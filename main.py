import requests
import datetime
import base64
import urllib3
import os
from time import sleep
urllib3.disable_warnings()
status = {
    'elasticsearch': "",
    'kibana': ""
}
print("startup")
while True:
    response = requests.get("{}/api/tunnels".format(os.getenv("NGROK_URL"))).json()
    urls = {}
    for tunnel in response["tunnels"]:
        if tunnel["public_url"].split(":")[0] == "https":
            urls[tunnel["name"]] = tunnel["public_url"]
    if urls['elasticsearch'] != status['elasticsearch']:
        print("{} - updating status".format(datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")))
        status['elasticsearch'] = urls['elasticsearch']
        token = os.getenv("K8S_TOKEN")
        url_bytes = "{}:443".format(urls['elasticsearch']).encode('ascii')
        base64_bytes = base64.b64encode(url_bytes)
        secret = {
          'data': {
            'ELASTICSEARCH_URL': '{}'.format(base64_bytes.decode('ascii')),
          }
        }
        response = requests.patch("{}/api/v1/namespaces/elk/secrets/elasticsearch-cred".format(os.getenv("K8S_URL")),
                                 verify=False,
                                 headers={'Authorization': 'Bearer {}'.format(token),
                                          'Content-Type': "application/strategic-merge-patch+json"},
                                 json=secret
                                 )
        if response.status_code == 200:
            print("updated secret with url: {}:443".format(urls['elasticsearch']))
        else:
            print("error updating secret. Error is {}".format(response.json()))
        patch = {
            'spec': {
                'template': {
                    'metadata': {
                        'labels': {
                            'redeploy': '{}'.format(datetime.datetime.now().strftime("%Y-%m-%d-%M-%S"))
                        }
                    }
                }
            }
        }
        response = requests.patch("{}/apis/apps/v1/namespaces/elk/daemonsets/metricbeat".format(os.getenv("K8S_URL")),
                                 verify=False,
                                 headers={'Authorization': 'Bearer {}'.format(token),
                                          'Content-Type': "application/strategic-merge-patch+json"},
                                 json=patch
                                 )
        if response.status_code == 200:
            print("redeployed metricbeat")
        else:
            print("error updating metricbeat. Error is {}".format(response.json()))
        response = requests.patch("{}/apis/apps/v1/namespaces/elk/daemonsets/filebeat".format(os.getenv("K8S_URL")),
                                 verify=False,
                                 headers={'Authorization': 'Bearer {}'.format(token),
                                          'Content-Type': "application/strategic-merge-patch+json"},
                                 json=patch
                                 )
        if response.status_code == 200:
            print("redeployed filebeat")
        else:
            print("error updating filebeat. Error is {}".format(response.json()))
        response = requests.patch("{}/apis/apps/v1/namespaces/elk/deployments/apm".format(os.getenv("K8S_URL")),
                                 verify=False,
                                 headers={'Authorization': 'Bearer {}'.format(token),
                                          'Content-Type': "application/strategic-merge-patch+json"},
                                 json=patch
                                 )
        if response.status_code == 200:
            print("redeployed apm")
        else:
            print("error updating apm. Error is {}".format(response.json()))
    print("{} - sleeping an hour".format(datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S")))
    sleep(600)
